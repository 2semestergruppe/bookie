



#  2. Semester Projekt (Webudvikling)
Igennem hele semestret skal vi arbejde på et frontend projekt. Projektet danner grundlag for vores eksamensrapport i slutningen af semestret.

| Navn        | Rolle           | GitLab  |
| ------------- |-------------| -----|
| Mikkel Bang      | Scrum Master | https://gitlab.com/BangBang300 |
| Peter Eilerskov      | Scrum Team      |   https://gitlab.com/eilerskov |
| Nikolai Raahauge | Scrum Team     |    https://gitlab.com/nikolai001 |
| Casper Hauge | Scrum Team      |    https://gitlab.com/csr.hauge |

## Projektbeskrivelse
![alt text](https://i.imgur.com/K20ypMJ.png "Logo Title Text 1")

I det eksisterende bogmarked er det forlagene og anmeldelser der styrer hvilke bøger der popper op i butiksvinduer og reklamer.

Vi vil lave en platform, hvor brugeren, kan finde en ny bog at læse, som de ikke nødvendigvis kan finde på enhver boghylde.
Derfor stiller vi os selv følgende spørgsmål -
* Hvordan kan læsere blive introduceret for mindre kendte bøger?
* Hvordan får vi introduceret mindre kendte forfattere til læseren?




## Projektplan



### Projektstyring
Vi vil benytte os af Scrum og Agile scrum artifacts - Issue board: https://gitlab.com/2semestergruppe/bookie/-/boards.

### Sådan kører du projektet
<ol>
  <li>Klon projektet til dit ønskede directory</li>
  <li>Skriv "npm install" i terminalen</li>
  <li>Skriv "npm run serve" i terminalen</li>
  <li>Klik på " - Local:   http://localhost:xxx/"</li>
</ol>


## Brug af API

### **New york Times**

Fetch data fra New york Times med Javascript, der er forskellige muligheder for hvilken liste man ønsker at hente, vi har valgt at
bruge deres fulde liste.

Til dette skal du bruge en udvikler key inde fra deres side, se eventuelt https://developer.nytimes.com/get-started og https://developer.nytimes.com/docs/books-product/1/overview

```javascript

async created(){
  try{
    const response = await fetch("https://api.nytimes.com/svc/books/v3/lists/full-overview.json?api-key=JjiKdRB8tbswGr4iLOXSlP0WHIDttJsY")
    return await response.json().then(data => this.books = data.results.lists[0]);
  }catch(error){
    throw error;
  }
}
```
##### Et eksempel på vores kald til API, vi trækker dataen gennem et promise lige så snart created() køres og gemmer det derefter til et array kaldet books, for at kunne gøre dette skal du lave et array i din data sektion således;

```javascript
data(){
  return{
    books:[],
 };
```

### **Google Books**

Fetch data fra Google Books med Javascript, der er forskellige filtreringsvalg alt afhængigt af hvad man ønsker at finde, vi har valgt at filtrere på genre samt titler i vores eksempler.

Det er anbefalet men ikke nødvendigt at du får en API key indefra https://console.cloud.google.com/apis/credentials samt læser dokumentationen indefra google selv https://developers.google.com/books/docs/v1/using

```javascript

  async created() {
    try {
      const response = await fetch(
        "https://www.googleapis.com/books/v1/volumes?q=subject:fiction&maxResults=40&key=AIzaSyDoFqtwvCm45MiYFwPwquinYSWPy4H1koo"
      );
      return await response.json().then((data) => (this.books = data.items));
    } catch (error) {
      throw error;
    }
  },

  ```

  ##### Et eksempel på vores indledende kald til API, ligesom tidligere eksempel trækker vi dataen gennem et promise og vi gemmer det derefter til et array kaldet books, skal du bruge dette API sammen med tidligere eksempel i samme komponent skal du lave et nyt array eller vil der komme konflikt mellem dataen, se tidligere eksempel for at opsætte data
  <br>

  #### Opsætning af forskellige filtreringsmetoder med Google Books

  Alt efter ?q= og indtil &key i dit link til Google Books APIet er til filtrering, vær opmærksom på at hvis du ønsker at filtrere på flere af de samme kriterier e.g subject så skal hver filtreres med et + således;

  ```json
  q=subject:fiction+subject:horror&key=1234
  ```

  Ønsker du derimod at sortere på forskellige kriterier skal du separere disse med et & således;

  ```json
  q=subject:fiction&author:stephen&key=1234
  ```

  Udover dette bør et mellemrum repræsenteres af et + således;

  ```json
  q=author:stephen+king&key=1234
  ```
  Skulle der ønkes at søges efter en søgestring er dette også en simpel ting at sætte op således;

  ```javascript
  "https://www.googleapis.com/books/v1/volumes?q=Min+string&maxResults=30&key=AIzaSyDoFqtwvCm45MiYFwPwquinYSWPy4H1koo"
  ```
  
  Eller ved et input;

  ```javascript
  "https://www.googleapis.com/books/v1/volumes?q=" +
  input +
  "&maxResults=30&key=AIzaSyDoFqtwvCm45MiYFwPwquinYSWPy4H1koo"
  ```
  ##### input bliver sendt gennem en funktion
