import { setActivePinia, createPinia } from "pinia";
import { useBookStore } from "@/store/useBook.js";

describe("Book Store, useBook.js", () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it("checks if store is empty and if updateBook function works", () => {
    const wrapper = useBookStore();
    expect(wrapper.Title).toBe("");
    wrapper.updateBook("test", "test", "test", "test", "test", "test");
    expect(wrapper.Title).toBe("test"),
      expect(wrapper.Author).toBe("test"),
      expect(wrapper.BuyLink).toBe("test"),
      expect(wrapper.ISBN).toBe("test"),
      expect(wrapper.Description).toBe("test"),
      expect(wrapper.Img).toBe("test");
  });
});
