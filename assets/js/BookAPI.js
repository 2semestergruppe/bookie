google.books.load();
const SearchButton = document.querySelector("#Search");
const ResultsContainer = document.querySelector("#ResultContainer");
const CurrentSearch = document.querySelector("#CurrentSearch");

  //viewer.load('ISBN:0738531367');

  async function Search(){
    let SearchInput = document.querySelector("#SearchBar").value;
    if (!SearchInput.trim().length == false){
    // let Results = fetch("https://www.googleapis.com/books/v1/volumes?q="+SearchInput)

        ResultArray = ResultsContainer.querySelectorAll(".Item")
        
        ResultArray.forEach(Item => {
            ResultsContainer.removeChild(Item);
        });

        let response = await fetch("https://www.googleapis.com/books/v1/volumes?q="+SearchInput)
        //https://www.googleapis.com/books/v1/volumes?q=Harry_Potter&langRestrict=en
        //https://www.googleapis.com/books/v1/volumes?q=SearchInput+"&"+"langRestrict="+FilteredLanguage
        .then(response => response.json());
        let data = await response
        console.log(data);
        for(i = 0; i<data.items.length; i++){
            
            const NewItem = document.createElement('div')
            NewItem.classList.add('Item')

            const ItemTitle = document.createElement('h5')
            ItemTitle.innerHTML = data.items[i].volumeInfo.title;


            const ItemAuthor = document.createElement('p');
            ItemAuthor.innerHTML = data.items[i].volumeInfo.authors;

            if ( typeof data.items[i].volumeInfo.imageLinks != "undefined"){
                const ItemImage = document.createElement('img')
                ItemImage.src = data.items[i].volumeInfo.imageLinks.smallThumbnail
                if(typeof data.items[i].volumeInfo.authors != "undefined"){
                    ItemImage.alt = data.items[i].volumeInfo.title+" by "+data.items[i].volumeInfo.authors
                }else{
                    ItemImage.alt = data.items[i].volumeInfo.title
                }
                NewItem.appendChild(ItemImage)
            }
            NewItem.appendChild(ItemTitle);

            if ( typeof data.items[i].volumeInfo.authors != "undefined"){
                NewItem.appendChild(ItemAuthor);
            }

            ResultsContainer.appendChild(NewItem)

            CurrentSearch.innerHTML = "Showing results for "+SearchInput
        
        }
    }
}

document.querySelector("#SearchBar").addEventListener("keyup",function(event){
    if (event.keyCode == 13){
        event.preventDefault();
        Search();
    }
})

SearchButton.addEventListener("click", function(){
    Search();
});



//google.books.setOnLoadCallback(initialize);

//google.books.load();

//function initialize() {
  //let SearchInput = document.querySelector("#SearchBar").innerHTML
  //fetch("https://www.googleapis.com/books/v1/volumes?q="+SearchInput)
  //.then(response => response.json())
  //.then(data => console.log(data)); 
//}