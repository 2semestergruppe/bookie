import { defineStore } from "pinia";

export const useBookStore = defineStore("BookData", {
  state: () => ({
    Title: "",
    Author: "",
    BuyLink: "",
    ISBN: "",
    Description: "",
    Img: "",
  }),
  actions: {
    //Updates the stores properties with arguments sent by categories and trending and hero
    updateBook(title, author, link, isbn, desc, img) {
      (this.Title = title),
        (this.Author = author),
        (this.BuyLink = link),
        (this.ISBN = isbn),
        (this.Description = desc),
        (this.Img = img);
    },
  },
});
